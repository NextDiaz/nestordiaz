﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatronCommand
{
    class LucesCortas : LucesReceiver
    {
        private const int DISTANCIA = 40;

        public override int Encender()
        {
            this.encendidas = true;
            return DISTANCIA;
        }
    }
}
